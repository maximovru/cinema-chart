import {
    GET_MOVIES_LIST,
    CALLBACK_GET_MOVIES_LIST,
    SELECT_MOVIE,

    SELECT_PERIOD,

    GET_MOVIE_DATA,
    CALLBACK_MOVIE_DATA,
    CALLBACK_NEW_MOVIE_DATA,

    SELECT_MOVIES,
    CALLBACK_COMPARE_MOVIE_DATA,
    SELECT_CHART_TYPE,
    SELECT_POINT,

    CALLBACK_TWEETS_LINE
} from '../constants/ActionTypes';

import * as store from 'store';
import { socket } from '../store/subscribeStore';

/* ---------- MovieList ------------- */

export function getMovieList() {
    return () => socket.emit('getMovieList');
}
export function callbackGetMovieList(movies) {
    return {
        type: CALLBACK_GET_MOVIES_LIST,
        payload: movies
    }
}

export function selectMovie(id) {
    return {
        type: SELECT_MOVIE,
        payload: id
    }
}

export function selectCompareMovie(id) {
    return {
        type: ADD_MOVIE_TO_SELECT,
        payload: id
    }
}

export function deselectCompareMovie(id) {
    return {
        type: REMOVE_MOVIE_FROM_SELECT,
        payload: id
    }
}

export function selectGraphicType(id) {
    return {
        type: CHANGE_GRAPHIC_TYPE,
        payload: id
    }
}

/* ---------- Date Range ------------- */

export function selectPeriod(datePeriod) {
    return {
        type: SELECT_PERIOD,
        payload: datePeriod
    }
}

/* ---------- Statistic ------------- */

export function getMovieData(data) {
    return () => socket.emit('getDataForMovie', data);
}

export function callbackMovieData(data) {
    return {
        type: CALLBACK_MOVIE_DATA,
        payload: data
    }
}

export function callbackNewMovieData(data) {
    return {
        type: CALLBACK_NEW_MOVIE_DATA,
        payload: data
    }
}

/* ---------- Compare Movie Data ------------- */

export function getDataCompareForMovies(data) {
    return () => socket.emit('getDataCompareForMovies', data);
}

export function selectMovies(data) {
    return {
        type: SELECT_MOVIES,
        payload: data
    }
}

export function selectChartType(data) {
    return {
        type: SELECT_CHART_TYPE,
        payload: data
    }
}

export function callbackCompareMovieData(data) {
    return {
        type: CALLBACK_COMPARE_MOVIE_DATA,
        payload: data
    }
}


export function selectPoint(data) {
    return () => socket.emit('getTweetline', data);
}

export function callbackSelectPoint(data) {
    console.log(data);
    return {
        type: CALLBACK_TWEETS_LINE,
        payload: data
    }
}
