var webpack = require('webpack');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');
var wpConfig = require('./webpack.config');
var mainConfig = require('./main.config');



var port = mainConfig.webServer.port;
http = require('http');
var app = require('express')();
var server = http.createServer(app);
var io = require('socket.io')(server);

var session = require("express-session")({
    secret: "my-secret",
    resave: true,
    saveUninitialized: true
});
var sharedSession = require("express-socket.io-session");


server.listen(port,mainConfig.webServer.ip,
    function(error) {
      if (error) {
        console.error(error)
      } else {
        console.info("==> Listening on port %s. Open up http://localhost:%s/ in your browser.", port, port)
      }
    });

io.use(sharedSession(session, {
    autoSave:true
}));

var IoController = require('./controllers/io.controller');
var ioc = new IoController(io);

app.use(session);
var compiler = webpack(wpConfig);
app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: wpConfig.output.publicPath }));
app.use(webpackHotMiddleware(compiler));

app.get("/", function(req, res) {
    console.log('session req:',req.session.id,req.session.userdata);
  res.sendFile(__dirname + '/index.html')
});
