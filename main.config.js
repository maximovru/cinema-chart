module.exports = {
    webServer: {
        ip: "0.0.0.0",
        port: 3000
    },
    cassandraDb: {
        contactPoints: ['172.22.4.36:31056']//['172.22.4.122:31593']
    },
    COUNT_GRAPHIC_POINTS_MAX: 100
};