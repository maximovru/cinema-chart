import { combineReducers } from 'redux';
import movieList from './MovieList';
import dateRange from './DateRange';
import statistic from './Statistic';
import movieData from './MovieData';
import compareMovies from './CompareMovies';
import tweetsLine from './TweetsLine';

const rootReducer = combineReducers({
    movieList,
    dateRange,
    statistic,
    movieData,
    compareMovies,
    tweetsLine
})

export default rootReducer;
