import {
    CALLBACK_GET_MOVIES_LIST,
    SELECT_MOVIE
} from '../constants/ActionTypes';
import { socket } from '../store/subscribeStore';

const initialState = {
    movies: [],
    selected: ''
};

export default function movieList(state = initialState, action) {
    switch (action.type) {
        case CALLBACK_GET_MOVIES_LIST:
            return Object.assign({}, state, {movies: action.payload, selected: action.payload[0].id});

        case SELECT_MOVIE:
            return Object.assign({}, state, {selected: action.payload});

        default:
            return state;
    }
}

