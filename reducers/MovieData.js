import {
    CALLBACK_MOVIE_DATA,
    CALLBACK_NEW_MOVIE_DATA
} from '../constants/ActionTypes';

const initialState = [];

export default function MovieData(state = initialState, action) {
    switch (action.type) {
        case CALLBACK_MOVIE_DATA:
            return [].concat(action.payload);

        default:
            return state;
    }
}

