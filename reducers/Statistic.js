import {
    CALLBACK_NEW_MOVIE_DATA,
    SELECT_MOVIE
} from '../constants/ActionTypes';
import moment from 'moment';

let fieldNames = ['tweetCounterNegative','tweetCounterPositive','viewCounterNegative','viewCounterPositive'];
const firstData = {
    timestamp: moment().format('X'),
    tweetCounterNegative: 0,
    tweetCounterPositive: 0,
    viewCounterNegative: 0,
    viewCounterPositive: 0
};

const initialState = {
    statistic:[firstData],
    lastStatElement: firstData,
    diffLastElement: firstData
};

function calculateStatDiff(oldStat, newStat){
    let diff = {};
    for(var i = 0; i < fieldNames.length; i++){
        let fieldName = fieldNames[i];
        diff[fieldName] = newStat[fieldName] - oldStat[fieldName];
    }
    return diff;
}

export default function Statistic(state = initialState, action) {
    switch (action.type) {
        case CALLBACK_NEW_MOVIE_DATA:
            return Object.assign(
                {},
                state,
                {
                    statistic: state.statistic.concat(action.payload),
                    lastStatElement: action.payload,
                    diffLastElement: calculateStatDiff(state.lastStatElement,action.payload)
                }
            );

        case SELECT_MOVIE:
            firstData.timestamp = moment().format('X');
            return {
                statistic:[firstData],
                lastStatElement: firstData,
                diffLastElement: firstData
            };

        default:
            return state;
    }
}
