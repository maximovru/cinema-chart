import {
    CALLBACK_COMPARE_MOVIE_DATA,
    SELECT_MOVIES,
    SELECT_MOVIE,
    SELECT_CHART_TYPE
} from '../constants/ActionTypes';
import moment from 'moment';

const initialState = {
    selectedMovies: [],
    movieDataMap: {},
    selectedChartType: 'line_all'
};

export default function CompareMovies(state = initialState, action) {
    switch (action.type) {
        case SELECT_MOVIES:
            return Object.assign({}, state, {selectedMovies: action.payload});

        case CALLBACK_COMPARE_MOVIE_DATA:
            return Object.assign({}, state, {movieDataMap: action.payload});

        case SELECT_CHART_TYPE:
            return Object.assign({}, state, {selectedChartType: action.payload});

        default:
            return state;
    }
}
