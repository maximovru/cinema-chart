import {
    SELECT_PERIOD
} from '../constants/ActionTypes';

import { socket } from '../store/subscribeStore';
import moment from 'moment';

const now = moment();

now.locale('en-gb');

const fromDate = now.clone().subtract(1, 'week').startOf('day');
const toDate = now.clone().endOf('day');

const initialState = [
    fromDate,
    toDate,
];

export default function dateRange(state = initialState, action) {
    switch (action.type) {
        case SELECT_PERIOD:
            return action.payload;

        default:
            return state;
    }
    return state;
}
