import {
    CALLBACK_TWEETS_LINE,
    CALLBACK_NEW_PART_TWEETS
} from '../constants/ActionTypes';

const initialState = {
    page_num: 0,
    pages: 0,
    tweets: []
};

export default function TweetsLine(state = initialState, action) {
    switch (action.type) {
        case CALLBACK_TWEETS_LINE:
            return Object.assign({},
                state,
                {page_num: action.payload.page_num},
                {pages: action.payload.pages},
                {tweets: action.payload.tweets}
            );

        case CALLBACK_NEW_PART_TWEETS:
            return Object.assign({},
                state,
                {page_num: action.payload.page_num},
                {pages: action.payload.pages},
                {tweets: state.tweets.concat(action.payload.tweets)}
            );

        default:
            return state;
    }
}

