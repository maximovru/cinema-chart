/**
 * Created by omaximov on 16/06/16.
 *
 * This controller manage socket.io events.
 */
'use strict';
console.log('require io.controller ');
var StatSubscribe = require('../models/stat.subscribe');
var TweetStat = require('../models/tweet.stat');
var TweetMessage = require('../models/tweet.message');
var Movie = require('../models/movie');
var ApiManager = require('../services/api.manager');
var async = require('async');

//console.log('require io.controller2 ',UserList);0

var events = {
    getMovieList: function (socket, data) {
        Movie.getAllList().then(function (movieList){
            ApiManager.sendMovieList(socket, movieList);
        });

    },
    getDataForMovie: function (socket, data) {
        TweetStat.getStatByMovieId(data['movieId'],data['fromTs'],data['toTs']).then(function(tweetStats){
            //console.log('tw',tweetStats);
            StatSubscribe.getInstance().unsubscribeAnyStat(socket.id);
            //if(){//todo: reverse logic
                StatSubscribe.getInstance().subscribeNewStat(
                    5,
                    socket.id,
                    data['movieId'],
                    function(tweetStat){
                        if(tweetStat) {
                            ApiManager.sendOneTweetStatistic(socket, tweetStat);
                        }
                    }
                );
            //}
            ApiManager.sendTweetStatistic(socket,tweetStats);
        });
    },
    getDataCompareForMovies: function (socket, data) {
        var taskNamed = {};
        for(var i =0; i < data['movies'].length; i++){
            var movieId = data['movies'][i];
            taskNamed[movieId] = (function(movie){
                console.log('create function for movie '+movie);
                return function(callback){
                    console.log('run function for movie '+movie);
                    TweetStat.getStatByMovieId(movie,data['fromTs'],data['toTs']).then(function(tweetStats){
                        console.log('result for movie:'+movie+' cnt'+tweetStats.length);
                        callback(null,tweetStats);
                    })
                }
            })(movieId)
        }
        async.parallel(taskNamed,function(err,movieData){
            console.log('sendAll stat');
            ApiManager.sendCompareTweetStatistic(socket,movieData);
        });
    },
    getTweetline: function (socket, data) {
        TweetMessage.getTweets(data['movie'],data['fromTs'],data['toTs'],data['perPage'],data['page']).then(
            function (res){
                console.log('result of tweetline',res);
                ApiManager.sendTweetline(socket, res);
            }
        );
    }
};
class IoController {
    constructor(io) {
        this.io = io;
        io.on(
            'connection',
            socket => {
                console.log('self:');
                this.connected(socket);
                socket.on('disconnect',
                    ()=> {
                        this.disconnected(socket);
                    }
                );
            }
        );
        ApiManager.setIo(io);
    }

    connected(socket) {
        for(let eventName in events) {
            console.log('set ' + eventName + ' event on ', socket.id);
            socket.on(eventName,
                (
                    function (eventName) {
                        return function (userdata) {
                            console.log('on '+ eventName, userdata);
                            events[eventName](socket, userdata)
                        }
                    }
                )(eventName)
            )
        }
        //connected action
        console.log('connected action')
    }

    disconnected(socket) {
        //disconnected action
        console.log('disconnected action',socket.handshake.session.username);
        StatSubscribe.getInstance().unsubscribeAnyStat(socket.id);

    };
}


module.exports = IoController;
