import {
    callbackGetMovieList,
    callbackNewMovieData,
    callbackMovieData,
    callbackCompareMovieData,
    callbackSelectPoint
} from '../actions/PageActions';
var moment = require('moment');

export const socket = window.io();

export function init(store) {
    /**
     * @param  {object} response
     * @return {[type]}
     */
    socket.on('movieList', (response) => {
        store.dispatch(callbackGetMovieList(response));
    });

    window.gSocket = socket;
    window.moment = moment;
    /**
     * @param  {array} users
     * @return {[type]}
     */
    socket.on('tweetStatistic', (statistic) => {
        store.dispatch(callbackMovieData(statistic));
    });

    socket.on('tweetCompareStatistic', (statistic) => {
        store.dispatch(callbackCompareMovieData(statistic));
    });

    socket.on('newTweetStatistic', (statistic) => {
        store.dispatch(callbackNewMovieData(statistic));
    });

    socket.on('tweetline', (tweets) => {
        store.dispatch(callbackSelectPoint(tweets));
    });

}
