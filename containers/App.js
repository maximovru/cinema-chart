import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import MovieList from '../components/MovieList';
import DateRange from '../components/DateRange';
import DateRangeLine from '../components/DateRangeLine';
import Statistic from '../components/Statistic';
import RealtimeStatistic from '../components/Realtime';

import Chart_1 from '../components/Chart_1';
import Chart_2 from '../components/Chart_2';
import Chart_3 from '../components/Chart_3';
import Chart_4 from '../components/Chart_4';
import Chart_bubble from '../components/Chart_bubble';

import CompareList from '../components/CompareList';
import CompareCharts from '../components/CompareCharts';
import ChartType from '../components/CompareChartType';

import TweetsLine from '../components/TweetsLine';

import * as pageActions from '../actions/PageActions';

class App extends Component {

    render() {
        const {
            movies,
            selected
        } = this.props.movieList;

        const movieData = this.props.movieData;

        const tweetsLine = this.props.tweetsLine;

        const {
            selectedMovies,
            movieDataMap,
            selectedChartType
        } = this.props.compareMovies;

        const {
            getMovieList,
            selectMovie,
            selectPeriod,
            getMovieData,

            getDataCompareForMovies,
            selectMovies,
            selectChartType,

            selectPoint

        } = this.props.pageActions;

        const { dateRange } = this.props;

        return (
            <div className="main">
                <div className="frame">
                    <div className="side">
                        <div className="movie-list--section block">
                            <MovieList
                                movies={ movies }
                                getMovieList={ getMovieList }
                                selectMovie={ selectMovie }
                                selected={ selected }
                            />
                        </div>
                        <div className="tweets-list--section block">
                            <TweetsLine
                                tweets={ tweetsLine.tweets }
                            />
                        </div>
                    </div>
                    <div className="content">
                        <div className="statistic-section block">
                            <Statistic
                                getMovieData={ getMovieData }
                            />
                            <RealtimeStatistic />
                        </div>
                        <div className="block">
                            <label className="title">Historical data</label>
                            <div className="date-range-section">
                                <DateRange
                                    getMovieData={ getMovieData }
                                    movieId={ selected }
                                    dateRange={ dateRange }
                                    selectPeriod={ selectPeriod }
                                />
                            </div>
                            <div className="chart-1-section">
                                <Chart_1
                                    movieData={ movieData }
                                    selectPoint={ selectPoint }
                                    movieId={ selected }
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="frame">
                    <div className="block">
                        <label className="title">
                            Check how the same data is shown with different diagrams:<br/>
                            which one resonates more with the story behind the data?<br/>
                        </label>
                        <h3>Selected period <DateRangeLine
                            getMovieData={ getMovieData }
                            movieId={ selected }
                            dateRange={ dateRange }
                            selectPeriod={ selectPeriod }
                        /></h3>



                        <div className="chart-2-section quarter">
                            <Chart_2
                                movieData={ movieData }
                            />
                        </div>
                        <div className="chart-3-section quarter">
                            <Chart_3
                                movieData={ movieData }
                            />
                        </div>
                        <div className="chart-4-section quarter">
                            <Chart_4
                                movieData={ movieData }
                            />
                        </div>
                        <div className="chart_bubble-section quarter">
                            <Chart_bubble
                                movieData={ movieData }
                            />
                        </div>
                    </div>
                </div>
                <div className="frame">
                    <div className="side">
                        <div className="block">
                            <CompareList
                                movies={ movies }
                                getMovieCompare={ getDataCompareForMovies }
                                selectMovies={ selectMovies }
                                selected={ selectedMovies }
                                dateRange={ dateRange }
                            />
                        </div>
                        <div className="block">
                            <ChartType
                                selectedChartType={ selectedChartType }
                                selectChartType={ selectChartType }
                            />
                        </div>
                        <div className="block">
                            <DateRange
                                getMovieData={ getMovieData }
                                movieId={ selected }
                                dateRange={ dateRange }
                                selectPeriod={ selectPeriod }
                            />
                        </div>
                    </div>
                    <div className="content">
                        <div className="block">
                            <CompareCharts
                                movieDataMap={ movieDataMap }
                                selectedChartType={ selectedChartType }
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
/**
 * Set data types for App
 * @type {Object}
 */
App.propTypes = {
    movieList: PropTypes.object.isRequired,
}

/**
 * Binding state
 * @param  {obj}
 * @return {obj}
 */
function mapStateToProps(state) {
    return {
        movieList: state.movieList,
        dateRange: state.dateRange,
        movieData: state.movieData,
        compareMovies: state.compareMovies,
        tweetsLine: state.tweetsLine
    }
}

/**
 * Binding actions
 * @param  {function}
 */
function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators(pageActions, dispatch)
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)
