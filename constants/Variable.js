export const DATE_TIME = 'HH:mm:ss';
export const DATE_YEAR = 'YYYY-MM-DD';
export const DATE_MONTH = 'MMM D, YYYY';
// export const DATE_FULL = 'MMM D, YYYY';
