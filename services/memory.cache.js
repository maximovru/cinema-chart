/**
 * Created by omaximov on 27/09/16.
 */
'use strict';
var mainConfig = require('../main.config');

var instance = null;
var autoCleanerMap = {};
var store = {};

class MemoryCache {
    constructor(){
        if(!instance){
            instance = this;
        }

        return instance;
    }

    static getInstance(){
        return new MemoryCache();
    }

    getValue(key,addTTL){
        if(addTTL == undefined){
            addTTL = 0;
        }
        this.updateAutoClean(key, addTTL);
        return Promise.resolve(store[key]);
    }

    setValue(key,value,TTL){
        if(TTL == undefined){
            TTL = 10;
        }
        this.updateAutoClean(key, TTL);
        store[key] = value;
    }

    updateAutoClean(key, TTL){
        if(autoCleanerMap[key]){
            clearTimeout(autoCleanerMap[key].timeoutId);
        }
        if(autoCleanerMap[key] == undefined) {
            autoCleanerMap[key] = {ttl:0,timeoutId:0};
        }
        autoCleanerMap[key].ttl += TTL;
        autoCleanerMap[key].timeoutId = setTimeout(
            this.generateRemoveFunction(key),
            autoCleanerMap[key].ttl * 1000
        );

    }

    generateRemoveFunction(key){
        return function () {
            delete autoCleanerMap[key];
            delete store[key];
        }
    }
}

module.exports = MemoryCache;