'use strict';
var cassandra = require('cassandra-driver');
var async = require('async');
var mainConfig = require('../main.config');

var instance = null;
var client = null;
var client2 = null;

class CassandraDb {
    constructor(){
        console.log('constructor Api');
        if(!instance){
            instance = this;
            client =  new cassandra.Client({ contactPoints: mainConfig.cassandraDb.contactPoints});
            this.connected = false;
            this.connectedErr = false;

            client.connect(function (err) {
                if (err) {
                    instance.connectedErr = err;
                    client.shutdown();
                    return console.error('There was an error when connecting', err);
                }
                instance.connected = true;
            });

            client2 =  new cassandra.Client({ contactPoints: ['172.22.4.36:31056']
            });

            client2.connect(function (err) {
                if (err) {
                    //instance.connectedErr = err;
                    client2.shutdown();
                    return console.error('There was an error when connecting', err);
                }
                //instance.connected = true;
            });

        }

        return instance;
    }

    getClient(){
        var self = this;
        return new Promise(function(resolve, reject){
            if(self.connectedErr){
                reject(self.connectedErr);
            }else if(self.connected){
                resolve(client);
            }else{
                client.connect(function (err) {
                    if (err) {
                        instance.connectedErr = err;
                        client.shutdown();
                        return console.error('There was an error when connecting', err);
                    }
                    instance.connected = true;
                });
            }
        })
    }

    execute(query, data){
        var self = this;
        return new Promise(function(resolve, reject){
            async.series([
                function connect(next) {
                    console.log('cassandra try connect');
                    client.connect(next);
                },
                function selectingData(next){
                    client.execute(query, data, { prepare: true}, function (err, result) {
                        console.log('cassandra try exec');
                        if (err) return next(err);
                        //var row = result.first();
                        next(null, result.rows);
                    });
                }
                ],

                function(err, result){
                    if(err){
                        console.log('cassandra err',err);
                        reject(err);

                    }
                    resolve(result);

                }
            );
        });
    }

    executeAll(queries){
        var self = this;
        var namedTasks = {};
        for(var i=0;i<queries.length;i++){
            namedTasks[i] = (function(query){
                return function(callback){CassandraDb.getInstance().execute(query, []).then(function(rows){
                    var rs = rows[1];

                    callback(null,rs);
                })}
            })(queries[i])
        }

        return new Promise(function(resolve, reject){
            async.parallel(namedTasks,function(err,results){
                if(err){
                    reject(err);
                }
                var ret = [];
                if(results){
                    for(var key in results){
                        ret = ret.concat(results[key]);
                    }
                }
                resolve([ret.length,ret]);
            });
        });
    }

    execute2(query, data){
        var self = this;
        console.log('query:',query);
        return new Promise(function(resolve, reject){
            async.series([
                    function connect(next) {
                        console.log('cassandra try connect');
                        client2.connect(next);
                    },
                    function selectingData(next){
                        client2.execute(query, data, { prepare: true}, function (err, result) {
                            console.log('cassandra try exec');
                            if (err) return next(err);
                            //var row = result.first();
                            next(null, result.rows);
                        });
                    }
                ],

                function(err, result){
                    if(err){
                        console.log('cassandra err',err);
                        reject(err);

                    }
                    console.log('count result:',result[1].length);
                    resolve(result);

                }
            );
        });
    }

    executeAll2(queries){
        var self = this;
        var namedTasks = {};
        for(var i=0;i<queries.length;i++){
            namedTasks[i] = (function(query){
                return function(callback){CassandraDb.getInstance().execute2(query, []).then(function(rows){
                    var rs = rows[1];

                    callback(null,rs);
                })}
            })(queries[i])
        }

        return new Promise(function(resolve, reject){
            async.parallel(namedTasks,function(err,results){
                if(err){
                    reject(err);
                }
                var ret = [];
                if(results){
                    for(var key in results){
                        ret = ret.concat(results[key]);
                    }
                }
                resolve([ret.length,ret]);
            });
        });
    }

    static getInstance(){
        return new CassandraDb();
    }


}
module.exports = CassandraDb;