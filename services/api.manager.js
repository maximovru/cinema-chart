/**
 * Created by omaximov on 21/06/16.
 */
'use strict';

var socketIo = null;
var instance = null;

class ApiManager {
    constructor(){
        console.log('constructor Api');
        if(!instance){
            instance = this;
        }

        return instance;
    }

    static setIo(io) {
        socketIo = io;
    }

    static sendTweetStatistic(socket, tweetsStat){
        //console.log('tweets:', JSON.stringify(tweetsStat));
        socket.emit("tweetStatistic",tweetsStat);
    }

    static sendTweetline(socket, tweets){
        socket.emit("tweetline",tweets);
    }

    static sendCompareTweetStatistic(socket, tweetsStat){
        console.log('tweets compare:', JSON.stringify(tweetsStat));
        socket.emit("tweetCompareStatistic",tweetsStat);
    }

    static sendOneTweetStatistic(socket, tweetStat){
        console.log('Onetweet:', JSON.stringify(tweetStat));
        socket.emit("newTweetStatistic",tweetStat);
    }

    static sendMovieList(socket, movieList){
        console.log('movieList:', JSON.stringify(movieList));
        socket.emit("movieList",movieList);
    }
}
module.exports = ApiManager;
