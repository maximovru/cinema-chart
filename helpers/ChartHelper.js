import moment from 'moment';
import { DATE_MONTH } from '../constants/Variable';

class ChartHelper {
    constructor() {

    }

    static getLineOptions() {
        return {
            responsive: true,
            scales: {
                xAxes: [{
                    type: 'linear',
                    position: 'bottom',
                    ticks: {
                        callback: function(value, index, values) {
                            return moment(value, 'X').format(DATE_MONTH);
                        },
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            animation: false,
            tooltips: {
                enabled: true,
                callbacks: {
                    label: function(tooltipItems) {
                        return tooltipItems.yLabel || '0';
                    },
                    title: function(tooltipItems) {
                        return moment(tooltipItems[0].xLabel, 'X').format(DATE_MONTH);
                    }
                }
            }
        }
    }

    static getBarOptions() {
        return {
            responsive: true,
            scales: {
                xAxes: [{
                    ticks: {
                        callback: function(value, index, values) {
                            return moment(value, 'X').format(DATE_MONTH);
                        }
                    },
                    stacked: true
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            animation: false,
            tooltips: {
                enabled: true,
                callbacks: {
                    label: function(tooltipItems) {
                        return tooltipItems.yLabel || '0';
                    }
                }
            },
            onClick: function(event, points) {
                if (points.length) {
                    let index = points[0]._index;
                    let data = points[0]._chart.config.data.datasets[0].data[index];
                    console.log(data);
                }
            }
        }
    }

    static getBubbleOptions() {
        return {
            responsive: true,
            scales: {
                xAxes: [{
                    type: 'linear',
                    position: 'bottom',
                    ticks: {
                        callback: function(value, index, values) {
                            return moment(value, 'X').format(DATE_MONTH);
                        },
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            animation: false,
            tooltips: {
                enabled: true,
                callbacks: {
                    label: function(tooltipItems) {
                        return tooltipItems.yLabel || '0';
                    },
                    title: function(tooltipItems) {
                        return moment(tooltipItems[0].xLabel, 'X').format(DATE_MONTH);
                    }
                }
            },
            onClick: function(event, points) {
                if (points.length) {
                    let index = points[0]._index;
                    let data = points[0]._chart.config.data.datasets[0].data[index];
                    console.log(data);
                }
            }
        }
    }

    static datasetLine() {
        return {
            datasets: [
                {
                    label: 'Negative',
                    data: [{x: moment().format('X'), y: 0}],
                    lineTension: 0,
                    pointRadius: 3,
                    backgroundColor: 'rgba(255, 7, 23, 0.50)'
                },
                {
                    label: 'Positive',
                    data: [{x: moment().format('X'), y: 0}],
                    lineTension: 0,
                    pointRadius: 3,
                    backgroundColor: 'rgba(20, 163, 255, 0.50)'
                }
            ]
        }
    }

    static datasetBar() {
        return {
            labels: [moment().format('X')],
            datasets: [
                {
                    label: 'Negative',
                    data: [0],
                    backgroundColor: 'rgba(255, 7, 23, 0.50)',
                    hoverBackgroundColor: 'rgba(255, 7, 23, 0.75)'
                },
                {
                    label: 'Positive',
                    data: [0],
                    backgroundColor: 'rgba(20, 163, 255, 0.50)',
                    hoverBackgroundColor: 'rgba(20, 163, 255, 0.75)'
                }
            ]
        }
    }

    static datasetBubble() {
        return {
            datasets: [
                {
                    label: 'Negative',
                    data: [{x: moment().format('X'), y: 0, r: 0}],
                    backgroundColor: 'rgba(255, 7, 23, 0.50)',
                },
                {
                    label: 'Positive',
                    data: [{x: moment().format('X'), y: 0, r: 0}],
                    backgroundColor: 'rgba(20, 163, 255, 0.50)'
                }
            ]
        }
    }

    static processLineData(data) {
        let cleanData = {
            positive: [],
            negative: []
        };

        let positiveSum = 0;
        let negativeSum = 0;

        data.map((el, i) => {
            let pItem = {};
            let nItem = {};

            positiveSum += parseInt(el.tweetCounterPositive, 10);
            negativeSum += parseInt(el.tweetCounterNegative, 10);

            pItem.x = el.timestamp;
            pItem.y = positiveSum;
            pItem.period = el.period;

            nItem.x = el.timestamp;
            nItem.y = negativeSum;
            nItem.period = el.period;

            cleanData.positive.push(pItem);
            cleanData.negative.push(nItem);
        });

        return cleanData;
    }

    static processBarData(data) {
        let cleanData = {
            positive: [],
            negative: [],
            labels: []
        };

        data.map((el, i) => {
            let positive = parseInt(el.tweetCounterPositive, 10);
            let negative = parseInt(el.tweetCounterNegative, 10);
            let date = el.timestamp;
            // pItem.period = el.period;

            cleanData.positive.push(positive);
            cleanData.negative.push(negative);
            cleanData.labels.push(date);
        });

        return cleanData;
    }

    static processBubbleData(data) {
        let cleanData = {
            positive: [],
            negative: []
        };

        data.map((el, i) => {
            let positive = {
                x: el.timestamp,
                y: parseInt(el.tweetCounterPositive, 10),
                r: ~~(Math.random() * 10) + 1,
                period: el.period
            };

            let negative = {
                x: el.timestamp,
                y: parseInt(el.tweetCounterNegative, 10),
                r: ~~(Math.random() * 10) + 1,
                period: el.period
            };

            cleanData.positive.push(positive);
            cleanData.negative.push(negative);
        });

        return cleanData;
    }
}

export default ChartHelper;
