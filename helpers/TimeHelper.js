/**
 * Created by omaximov on 30/06/16.
 */
import moment from 'moment';
moment.locale('ru');

class TimeHelper {
    constructor() {

    }

    static getMessageTime(unixtimestamp) {
        //ut - 1469094685
        //cu - 1469094695
        window.th = TimeHelper;
        let messageMoment = moment.unix(unixtimestamp);
        let timeText = [
            'только что',
            'менее минуты назад',
            'пару минут назад',
            'fromNow',
            'сегодня в HH:mm',
            'DD.MM.YYYY HH:mm:ss'
        ];
        let timeouts = [10,60,300,3600];
        let currentTimestamp = moment().format("X");
        let tomorrow = moment().add(1,'d').hour(0).minute(0).seconds(0).format("X");
        timeouts.push((tomorrow - currentTimestamp));
        //console.log(timeouts);
        let from = -5;
        for(let i in timeouts) {
            let to = timeouts[i];
            //let to = timeout - (currentTimestamp - unixtimestamp);
            let delta = currentTimestamp - unixtimestamp;
            if((delta >= from ) && (delta < to)) {
                if(timeText[i] === 'fromNow') {
                    return messageMoment.fromNow();
                } else {
                    return messageMoment.format(timeText[i]);
                }
            }
            from = to;
        }
        return messageMoment.format(timeText[timeText.length - 1]);
    }

    static setRedrawTrigger(callback,unixtimestamp) {
        let timeoutIds = [];
        let timeouts = [10,60,300];//in seconds
        let temp = 300;
        while(temp < 3600) {
            temp += 60;
            timeouts.push(temp);
        }

        let currentTimestamp = moment().format("X");
        let tomorrow = moment().add(1,'d').hour(0).minute(0).seconds(0).format("X");
        timeouts.push((tomorrow - currentTimestamp));
        //console.log(timeouts);

        for(let i in timeouts) {
            let timeout = timeouts[i];
            let to = timeout - (currentTimestamp - unixtimestamp);
            if(to > 0) {
                timeoutIds.push(
                    setTimeout(
                        callback,
                        to * 1000
                    )
                );
            }

        }
        return timeoutIds;
    }




}
export default TimeHelper;