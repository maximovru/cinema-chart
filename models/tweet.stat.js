/**
 * Created by omaximov on 16/08/16.
 */
'use strict';
var CassandraDb = require('../services/cassandra.db');
var mainConfig = require('../main.config');
var moment = require('moment');
const PERIOD_HOUR = 3600;
const PERIOD_30SEC = 30;
const MIN_HOUR_POINTS = 60;
let lastTweetStats = {};
let lastTweetStatsDuration = {};
var fieldList = ['tweetCounterPositive','tweetCounterNegative','viewCounterPositive','viewCounterNegative'];

class TweetStat {
    constructor (timestamp, period, tweetCounterPositive, tweetCounterNegative, viewCounterPositive, viewCounterNegative, movieId) {
        this.movieId = movieId;
        this.period = period;
        this.timestamp = timestamp;
        this.tweetCounterPositive = tweetCounterPositive;
        this.tweetCounterNegative = tweetCounterNegative;
        this.viewCounterPositive = viewCounterPositive;
        this.viewCounterNegative = viewCounterNegative;
    }

    strId(){
        return this.movieId+';'+this.timestamp+';'+this.period;
    }

    isEmpty(){
        for(var i =0;i<fieldList.length;i++) {
            var fieldName = fieldList[i];
            if(this[fieldName]){
                return false;
            }
        }
        return true;
    }

    round(addObj,mul){
        if(!mul){
            mul = 1;
        }
        for(var i =0;i<fieldList.length;i++){
            var fieldName = fieldList[i];
            this[fieldName] += parseInt(addObj[fieldName])*mul;
        }
    }

    static getStatByMovieId(movieId, from, to) {
        from = parseInt(from);
        to = parseInt(to);
        if(from > to){
            var tmp = from;
            from = to;
            to = tmp;
        }
        var duration = 3600;

        var timePeriod = to - from;
        var splitDuration = TweetStat.getSplitDuration(from, to);

        var table = 'twitter_sentiment.metrics_1hr';
        if(splitDuration.period == PERIOD_30SEC ){
            duration = 30;
            table = 'twitter_sentiment.metrics_30sec';
        }


        return new Promise(function (resolve, reject) {
            var ret = [];
            if (!from) {
                var current = Math.round(new Date().getTime() / 1000);
                var startTimestamp = current;
            } else {
                var current = parseInt(from) + timePeriod;
                var startTimestamp = parseInt(from);
            }
            var query = 'SELECT movie, period_start_ts, negative_count, non_negative_count, negative_views, non_negative_views FROM '+table+' WHERE movie = ? AND period_start_ts > ? AND period_start_ts <= ?';
            var ts = moment(startTimestamp, 'X').format('YYYY-MM-DD HH:mm:ssZ');
            var endTs = moment((to), 'X').format('YYYY-MM-DD HH:mm:ssZ');
            console.log('timestamp',ts, startTimestamp,' end TS',endTs, 'query ',query);
            return CassandraDb.getInstance().execute(query, [movieId, ts,endTs]).then(
                function (rows) {
                    var rs = rows[1];

                    var newTweetStat = null;
                    var currentTime = from;
                    newTweetStat = new TweetStat(currentTime,splitDuration.duration,0,0,0,0,movieId);
                    for (var i in rs) {
                        var row = rs[i];
                        if (row) {
                            var ts = Math.round(row.period_start_ts.getTime() / 1000);
                            var stat = new TweetStat(ts, duration, row.non_negative_count, row.negative_count, row.non_negative_views, row.negative_views, movieId);
                            //in calculated period
                            if((stat.timestamp+stat.period - currentTime - splitDuration.duration) <= 0 ){
                                newTweetStat.round(stat, 1);
                            }else{//now start ne period
                                if(!newTweetStat.isEmpty()) {
                                    //save previous period statistic to result
                                    ret.push(newTweetStat);
                                }

                                var dlt = (ts - from) % splitDuration.duration;
                                currentTime = ts - dlt;
                                newTweetStat = new TweetStat(currentTime,splitDuration.duration,0,0,0,0,movieId);
                                newTweetStat.round(stat, 1);
                            }

                        } else {
                            console.log('strange:', i, rows[1][i])
                        }
                    }
                    if(!newTweetStat.isEmpty()) {
                        //save last period statistic to result
                        ret.push(newTweetStat);
                    }
                    //console.log('results:',ret.length);
                    resolve(ret);

                }
            );
        });
    }

    static getSplitDuration(from, to){
        var delta = to - from;
        var ret = 1;
        var period = 1;

        if(delta / PERIOD_HOUR < MIN_HOUR_POINTS){
            ret = PERIOD_30SEC;
            period = PERIOD_30SEC;
            ret = (Math.ceil(delta / (ret * mainConfig.COUNT_GRAPHIC_POINTS_MAX))) * PERIOD_30SEC;
        }else{
            ret = PERIOD_HOUR;
            period = PERIOD_HOUR;
            ret = (Math.ceil(delta / (ret * mainConfig.COUNT_GRAPHIC_POINTS_MAX))) * PERIOD_HOUR;
        }

        return {period:period, duration:ret};
    }

    static getLastStatByMovieIdGenerated(movieId, duration){
        var table = 'twitter_sentiment.metrics_30sec';

        var dt = 30;
        var ts = Math.round(new Date().getTime() / 1000) - dt - 5;
        console.log('lts:',ts,duration);
        ts = moment(ts, 'X').format('YYYY-MM-DD HH:mm:ssZ');
        console.log('lts2:',ts);
        var query = 'SELECT movie, period_start_ts, negative_count, non_negative_count, negative_views, non_negative_views FROM '+table+' WHERE movie = ? AND period_start_ts > ?';
        var ret = [];
        return CassandraDb.getInstance().execute(query, [movieId, ts]).then(
            function (rows) {
                var rs = rows[1];

                for (var i in rs) {
                    var row = rs[i];
                    if (row) {

                        //console.log(row);
                        var ts = Math.round(row.period_start_ts.getTime() / 1000);
                        //console.log('row:',Math.round(row.period_start_ts.getTime() /1000), typeof row.period_start_ts);
                        var stat = new TweetStat(ts, duration, row.non_negative_count, row.negative_count, row.non_negative_views, row.negative_views, movieId);
                        ret.push(stat);
                    } else {
                        console.log('strange:', i, rows[1][i])
                    }
                }
                ret.sort(function(a,b){return a.timestamp - b.timestamp;});
                if(!lastTweetStats[movieId]){
                    console.log('no anything');
                    lastTweetStats[movieId] = [];
                }
                var newTweetStat = new TweetStat((parseInt(moment().format('X'))-duration),duration,0,0,0,0,movieId);
                if(lastTweetStats[movieId].length && ret.length){
                    var lastInd = lastTweetStats[movieId].length - 1;

                    var lastId = lastTweetStats[movieId][lastInd].strId();

                    var addMod = false;
                    for(var i = 0; i< ret.length;i++){
                        console.log(lastId+'  ');
                        console.log(ret[i].strId()+'  ');
                        if(lastId == ret[i].strId()){
                            newTweetStat.round(lastTweetStats[movieId][lastInd],-1);
                            addMod = true;
                        }
                        if(addMod){
                            newTweetStat.round(ret[i]);
                        }
                    }
                }

                lastTweetStats[movieId] = ret;
                if(newTweetStat) {
                    return [newTweetStat];
                }else{
                    return [];
                }
            }
        );
    }

}
module.exports = TweetStat;