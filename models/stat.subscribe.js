/**
 * Created by omaximov on 17/08/16.
 */
'use strict';
//console.log('required userList');

//var User = require('./user');
var ApiManager = require('../services/api.manager');
var TweetStat = require('./tweet.stat');
var instance = null;
var statInstances = {};
var subscribers = {};

class StatSubscribe {
    constructor() {
        if (!instance) {
            instance = this;
            let api = new ApiManager();
        }

        return instance;
    }

    subscribeNewStat(duration, subId, movieId, cb) {
        var qHash = duration+';'+movieId;
        var self = this;
        if(!statInstances[qHash]){
            var obj = {};
            console.log('addTask every', duration, ' seconds');
            obj.instance = setInterval(
                function(){
                    self.handleOneStatInstance(duration, movieId);
                },
                duration*1000
            );
            obj.subscribers = {};
            statInstances[qHash] = obj;
            console.log('to stat instances');
        }
        var subHash = duration+';'+subId+';'+movieId;
        console.log('subhash',subHash);
        subscribers[subId] = {duration:duration, movieId:movieId};
        console.log('subhash1');
        statInstances[qHash].subscribers[subHash] = {
            cb:cb,
            lastExecute: Math.round(new Date().getTime() / 1000)
        };
        console.log('subhash2');
        console.log('instances:',statInstances[qHash]);
    }

    unsubscribeAnyStat(subId) {
        if(subscribers[subId]){
            this.unsubscribeNewStat(subscribers[subId].duration, subId, subscribers[subId].movieId);
            delete subscribers[subId]
        }
    }

    unsubscribeNewStat(duration, subId, movieId) {
        var subHash = duration+';'+subId+';'+movieId;
        var qHash = duration+';'+movieId;
        if(statInstances[qHash].subscribers[subHash]){
            delete statInstances[qHash].subscribers[subHash];
        }
        var count = 0;
        for(var j in statInstances[qHash].subscribers){
            count++;
        }
        if(count == 0){
            clearInterval(statInstances[qHash].instance);
            delete statInstances[qHash];
        }
    }

    handleOneStatInstance (duration, movie){
        console.log('handleOneStatInstance');
        TweetStat.getLastStatByMovieIdGenerated(movie,duration).then(function (newTweetStats){
            console.log('new tweets',newTweetStats.length);
            if(newTweetStats.length) {
                var qHash = duration + ';' + movie;
                console.log('qHash',qHash,statInstances);
                for (var j in statInstances[qHash].subscribers) {
                    console.log('cb',newTweetStats.length);
                    var cb = statInstances[qHash].subscribers[j].cb;
                    if (typeof cb == 'function') {
                        for(var i in newTweetStats){
                            var stat = newTweetStats[i];

                            cb(stat);
                            break;
                        }
                    }
                    statInstances[qHash].subscribers[j].lastExecute = Math.round(new Date().getTime() / 1000);
                }
            }
        });
    }

    static getInstance(){
        return new StatSubscribe();
    }

}

module.exports = StatSubscribe;
