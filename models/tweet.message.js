
'use strict';
var CassandraDb = require('../services/cassandra.db');
var MemoryCache = require('../services/memory.cache');
var moment = require('moment');

var fieldList = ['id','created','followers','is_negative','message','movie','user'];

class TweetMessage {
    constructor (rawData) {
        if(rawData['tweet']){
            var tweetObj = rawData['tweet'];
            for(var i=0;i<fieldList.length;i++) {
                var field = fieldList[i];

                if (tweetObj[field] != undefined) {
                    this[field] = tweetObj[field];
                }
            }
        }
        for(var i=0;i<fieldList.length;i++){
            var field = fieldList[i];
            if(rawData[field] != undefined) {
                this[field] = rawData[field];
            }
        }

    }

    static getTweets(movieId, from, to, per_page, page_num) {
        from = parseInt(from);
        to = parseInt(to);
        if(from > to){
            var tmp = from;
            from = to;
            to = tmp;
        }

        return new Promise(function (resolve, reject) {
            var ret = [];
            //var query = 'SELECT id FROM twitter_sentiment.timeline WHERE movie = ? AND time > ? AND time <= ?';

            var hashKey = [movieId, from, to].join(';');

            MemoryCache.getInstance().getValue(hashKey).then(
                function (ids) {
                    if(!ids){
                        console.log('MC empty', ids);
                        var datetimes = TweetMessage.splitIntervalsToDateAndTime(from, to);
                        var queries = TweetMessage.generateTimelineQueries(movieId,datetimes);
                        return CassandraDb.getInstance().executeAll2(queries).then(
                            function (rows) {
                                var rs = rows[1];
                                var idsList = [];
                                for (var i in rs) {
                                    var row = rs[i];
                                    if (row) {
                                        idsList.push(row.id);
                                    }
                                }
                                return Promise.resolve(idsList);
                            }
                        );
                    }else{
                        return Promise.resolve(ids);

                    }
                }
            ).then(
                function (ids) {
                    MemoryCache.getInstance().setValue(hashKey,ids,30);
                    var pages = Math.ceil(ids.length / per_page);
                    let toPos = (page_num ) * per_page;
                    if(toPos > (ids.length - 1)){
                        toPos = ids.length;
                    }
                    var reqId = ids.slice((page_num - 1) * per_page, toPos);
                    console.log('from:',(page_num - 1) * per_page,' to:',toPos,' cnt:',reqId.length);
                    reqId = reqId.map(function(val){return "'"+val+"'";});

                    var idsList = reqId.join(',');
                    var query = 'SELECT * FROM twitter_sentiment.tweets WHERE id IN ('+idsList+')';
                    return CassandraDb.getInstance().execute2(query, []).then(
                        function (rows) {
                            var rs = rows[1];
                            for (var i in rs) {
                                var row = rs[i];
                                if (row) {
                                    var tweet = new TweetMessage(row);
                                    ret.push(tweet);
                                }
                            }
                            resolve({
                                tweets: ret,
                                pages: pages,
                                page_num: page_num
                            });
                        }
                    )

                }
            );
        });
    }

    static generateTimelineQueries(movie, datetimes){
        var queries = [];
        var query = 'SELECT id FROM twitter_sentiment.timeline WHERE movie = ';
        var parts = ["'"+movie+"'"];
        var onlyDays = [];
        for(var i=0;i<datetimes.length;i++){
            var obj = datetimes[i];
            if(!obj.fromTime && !obj.toTime){
                onlyDays.push("'"+obj.date+"'");
            }else{
                parts.push("date = '"+obj.date+"'");
                if(obj.fromTime){
                    parts.push("time >= '"+obj.fromTime+"'");
                }
                if(obj.toTime){
                    parts.push("time <= '"+obj.toTime+"'");
                }
                queries.push(query+parts.join(' AND '));
                parts = ["'"+movie+"'"];
            }
        }
        if(onlyDays.length){
            parts.push("date IN ("+onlyDays.join(',')+")");
            queries.push(query+parts.join(' AND '));
        }
        return queries;
    }

    static splitIntervalsToDateAndTime(fromTs, toTs){
        var ret = [];
        var fromTime = moment(fromTs,'X');
        var toTime = moment(toTs,'X');
        fromTime.utcOffset(0);
        toTime.utcOffset(0);

        var startDay = fromTime.dayOfYear();
        var endDay = toTime.dayOfYear();
        if(endDay< startDay){
            var tmpM = fromTime.clone();
            tmpM.endOf("year");
            endDay += tmpM.dayOfYear();
        }
        var tmpM = fromTime.clone();
        for(var day=startDay;day<=endDay;day++){
            var obj = {};
            obj.date = tmpM.format('YYYY-MM-DD');
            tmpM.add(1,'d');
            ret.push(obj);
        }
        var startDate = fromTime.format('YYYY-MM-DD');
        var endDate = toTime.format('YYYY-MM-DD');
        for(var i=0;i<ret.length;i++){
            var obj = ret[i];
            if(obj.date == startDate){
                obj.fromTime = fromTime.format('HH:mm:ss');
            }
            if(obj.date == endDate){
                obj.toTime = toTime.format('HH:mm:ss');
            }
        }
        return ret;
    }

}
module.exports = TweetMessage;