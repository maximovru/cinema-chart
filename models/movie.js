/**
 * Created by omaximov on 23/09/16.
 */
'use strict';
var CassandraDb = require('../services/cassandra.db');
var moment = require('moment');

let lastTweetStats = {};
let lastTweetStatsDuration = {};
var fieldList = ['tweetCounterPositive','tweetCounterNegative','viewCounterPositive','viewCounterNegative'];

class Movie {
    constructor(id, name,imdb, releaseDate) {
        this.id = id;
        this.name = name;
        this.imdb = imdb;
        this.releaseDate = releaseDate;
    }

    static getAllList(){
        var query = 'SELECT * FROM twitter_sentiment.movies';
        return CassandraDb.getInstance().execute(query).then(
            function (rows) {
                var rs = rows[1];
                var ret = [];

                for (var i in rs) {
                    var row = rs[i];
                    if (row) {
                        ret.push(new Movie(row.title,row.title,row.rating,row.release));
                    }
                }
                return ret;
            }
        );

    }
}
module.exports = Movie;