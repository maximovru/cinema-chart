import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import RC2 from 'react-chartjs2';
import ChartHelper from '../helpers/ChartHelper';

let len = 0;

class Chart_2 extends Component {

    constructor(props, context) {
        super(props, context);

        this.chartOption = ChartHelper.getLineOptions();
        this.chartDataset = ChartHelper.datasetLine();
        this.processData = ChartHelper.processLineData;
    }

    componentDidUpdate() {
        return this.updateChart();
    }

    updateChart() {
        if (!this.props.movieData.length) {
            return false;
        }

        if (this.props.movieData.length !== len) {
            len = this.props.movieData.length;

            let cleanData = this.processData(this.props.movieData);

            this.chart_2 = this.refs['chart_2'].getChart();

            this.chart_2.data.datasets[0].data = cleanData.negative;
            this.chart_2.data.datasets[1].data = cleanData.positive;
            this.chart_2.update();

            return true;
        }

        return false;
    }

    render() {

        return (
            <div className="chart-2-box">
                <h2 className="center">Stacked area</h2>
                <RC2 ref='chart_2' data={ this.chartDataset } options={ this.chartOption } type='line' />
            </div>
        )
    }
}

export default Chart_2;
