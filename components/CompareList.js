import React, { Component, PropTypes } from 'react';

class CompareList extends Component {

    constructor(props, context) {
        super(props, context);

    }

    checkMovie(event) {
        let { selected } = this.props;
        let val = event.target.value;
        let index = selected.indexOf(val);

        if (index == -1) {
            if (selected.length < 2) {
                selected.push(val);
            }
        } else {
            selected.splice(index, 1);
        }

        if (selected.length == 2) {
            this.getMovieCompare(selected);
        }

        return this.props.selectMovies(selected);
    }

    getMovieCompare(movies) {
        let data = {
            movies: movies,
            fromTs: this.props.dateRange[0].format('X'),
            toTs: this.props.dateRange[1].format('X')
        };

        this.props.getMovieCompare(data);
    }

    render() {
        const {
            movies,
            selected
        } = this.props;

        return (
            <div>
                <h2>Select two movies to compare</h2>
                <div className="half-block">
                    <table className="movie-list" width="100%">
                        <thead>
                            <tr>
                                <th width="*">name</th>
                                <th width="50px">IMDb</th>
                                <th width="90px">release</th>
                            </tr>
                        </thead>
                        <tbody>
                            {movies.map((movie, i) => (
                                <tr key={ movie.id }>
                                    <td>
                                        <label className="label">
                                            <input
                                                type="checkbox"
                                                name="movies" 
                                                value={ movie.id }
                                                onChange={ this.checkMovie.bind(this) }
                                                checked={ selected.indexOf(movie.id) !== -1 }
                                                disabled={ selected.length == 2 && selected.indexOf(movie.id) == -1 }
                                            />
                                            <span className="right">{ movie.name }</span>
                                        </label>
                                    </td>
                                    <td>
                                        { movie.imdb.toFixed(1) }
                                    </td>
                                    <td>
                                        { movie.releaseDate }
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

CompareList.propTypes = {
    movies: PropTypes.array.isRequired,
    // getUsersList: PropTypes.func
}

export default CompareList;
