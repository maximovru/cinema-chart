import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import {
    DATE_TIME,
    DATE_MONTH
} from '../constants/Variable';

class TweetsLine extends Component {

    constructor(props, context) {
        super(props, context);
    }

    render() {
        const {
            tweets
        } = this.props;

        return (
            <div>
                <label className="title">Tweets</label>
                <ul className="tweets-list">
                    {tweets.map((tweet, i) => (
                        <li
                            key={ tweet.id }
                            className={tweet.is_negative ? 'tweet-item negative' : 'tweet-item positive'}
                        >
                            <span className="tweet-text">{ tweet.message }</span>
                            <span className="tweet-user">
                                <span className="tweet-date">
                                    { moment(tweet.created).format(DATE_MONTH) + ' at ' +
                                    moment(tweet.created).format(DATE_TIME) }
                                </span>
                                @{ tweet.user}
                            </span>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}

export default TweetsLine;
