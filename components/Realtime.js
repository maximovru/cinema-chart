import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

let lastHash = '';
let fieldNames = ['tweetCounterNegative','tweetCounterPositive','viewCounterNegative','viewCounterPositive'];

class RTS extends Component {

    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {
        // this.chart_1 = this.refs['chart_1'].getChart();
        // console.log(this.chart_1);
        // this.chart_1.options.elements.line.tension = 0;
    }

    shouldComponentUpdate(nextProps) {
        let hash = '';
        for(let i = 0; i < fieldNames.length; i++){
            let fieldName = fieldNames[i];
            hash += ' '+nextProps.lastStatElement[fieldName]+';'+nextProps.diffLastElement[fieldName];
        }
        if (lastHash != hash) {
            lastHash = hash;
            return true;
        }

        return false;
    }

    htmlByFieldName(fieldName, invertColor){
        let char = ' ';
        let color = 'white-change';
        if(this.props.diffLastElement[fieldName] > 0){
            char = '↑';
            color = 'green-change';
            if(invertColor){
                color = 'red-change';
            }
        }else if(this.props.diffLastElement[fieldName] < 0) {
            char = '↓';
            color = 'red-change';
            if(invertColor){
                color = 'green-change';
            }
        }

        return (
            <span className={'rts-result '+color}>
                {this.props.lastStatElement[fieldName]} {char}
            </span>
        )
    }

    render() {
        return (
            <div>
                <h2>Real time static</h2>
                <p></p>
                <div>
                    <div className="rts-line">positive tweets: {this.htmlByFieldName('tweetCounterPositive')}</div>
                    <div className="rts-line">negative tweets: {this.htmlByFieldName('tweetCounterNegative',true)}</div>
                    <div className="rts-line">positive views: {this.htmlByFieldName('viewCounterPositive')}</div>
                    <div className="rts-line">negative views: {this.htmlByFieldName('viewCounterNegative',true)}</div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        lastStatElement: state.statistic.lastStatElement,
        diffLastElement: state.statistic.diffLastElement
    }
}

export default connect(
    mapStateToProps
)(RTS)
