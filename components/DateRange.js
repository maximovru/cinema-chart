import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

import 'rc-calendar/assets/index.css';
import RangeCalendar from 'rc-calendar/lib/RangeCalendar';

import Picker from 'rc-calendar/lib/Picker';
import 'rc-time-picker/assets/index.css';

import enUS from 'rc-calendar/lib/locale/en_US';
import 'rc-time-picker/assets/index.css';
import TimePickerPanel from 'rc-time-picker/lib/Panel';

import moment from 'moment';
import 'moment/locale/en-gb';

const timePickerElement = <TimePickerPanel />;
const formatStr = 'YYYY-MM-DD';
 // const formatStr = 'YYYY-MM-DD HH:mm:ss';

function format(v) {
    return v ? v.format(formatStr) : '';
}

function isValidRange(v) {
    return v && v[0] && v[1];
}

class DateRange extends Component {
    constructor(props, context) {
        super(props, context);
    }

    disabledDate(current) {
        const date = moment().endOf('day');

        return current.isAfter(date);
    }

    onChange(value) {
        console.log('onChange', value[1].format(formatStr+'Z'), moment().format(formatStr+'Z'));
        let data = {
            movieId: this.props.movieId,
            fromTs: value[0].format('X'),
            toTs: value[1].format('X')
        };

        this.props.getMovieData(data);

        return this.props.selectPeriod(value);
    }

    onHover(value) {
        /*return value.length > 1
            ? [value[0].startOf('day'), value[1].endOf('day')]
            : [value[0].startOf('day')];*/
    }

    render() {
        let { dateRange } = this.props;

        const calendar = (
            <RangeCalendar
                dateInputPlaceholder={ ['start', 'end'] }
                locale={ enUS }
                showOk={ true }
                format={ formatStr }
                onChange={ this.onHover.bind(this) }
                disabledDate={ this.disabledDate }
            />
        );

        return (
            <div className="sub-block">
                <h2>Select date period</h2>
                <Picker
                  animation="slide-up"
                  calendar={ calendar }
                  value={ dateRange }
                  onChange={ this.onChange.bind(this) }
                >
                    {
                        ({ value }) => {
                            return (
                                <span>
                                    <button
                                        disabled={ false }
                                        readOnly
                                        className="btn-calendar"
                                    >
                                        {isValidRange(value) && `${format(value[0])} | ${format(value[1])}` || ''}
                                    </button>
                                </span>
                            )
                        }
                    }
                </Picker>
            </div>
        )
    }
}

export default DateRange;

// timePicker={ timePickerElement }
