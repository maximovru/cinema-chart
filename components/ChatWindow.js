import React, { Component, PropTypes } from 'react';
import MessageLine from '../components/MessageLine';


class ChatWindow extends Component {
    constructor(props, context) {
        super(props, context);
    }

    sendMsg(e) {
        e.preventDefault();

        const { participant, owner } = this.props;

        let text = this.refs.textInput.value;

        if (text.length) {
            this.props.sendMsg({text: text, participant: participant, owner: owner});
        }
    }

    back(e) {
        e.preventDefault();

        this.props.backToList();
    }

    render() {
        const { owner, messages, participant } = this.props;
        console.log(messages);

        return (
            <div className="chat-window">
                <div className="info-panel">
                    <button className="info-panel--back" onClick={ this.back.bind(this) }> &lt; back </button>
                </div>
                <ul className="messages-window">
                    {messages.map((msg, i) => {
                            return <MessageLine
                                message={ msg }
                                key={ msg.messageId }
                                owner={ owner }
                                participant={ participant }
                            />
                        }
                    )}
                </ul>
                <form>
                    <div className="ctrl-panel">
                        <label className="login-form--label-input">
                            <input type="text" placeholder="Сообщение" ref="textInput" className="text-input"/>
                        </label>
                        <button onClick={ this.sendMsg.bind(this) } className="send-btn">send</button>
                    </div>
                </form>
            </div>
        )
    }
}

ChatWindow.propTypes = {
    owner: PropTypes.string,
    messages: PropTypes.array,
    participant: PropTypes.object
}

export default ChatWindow;
