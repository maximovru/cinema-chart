import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import RC2 from 'react-chartjs2';
import { DATE_TIME } from '../constants/Variable';

let chartData = {
    datasets: [
        {
            label: 'Negative',
            data: [{x: 0, y: 0}],
            lineTension: 0,
            pointRadius: 3,
            backgroundColor: 'rgba(255, 7, 23, 0.50)',
        },
        {
            label: 'Positive',
            data: [{x: 0, y: 0}],
            lineTension: 0,
            pointRadius: 3,
            backgroundColor: 'rgba(20, 163, 255, 0.50)'
        }
    ]
};

let chartOptions = {
    responsive: true,
    scales: {
        xAxes: [{
            type: 'linear',
            position: 'bottom',
            ticks: {
                callback: function(value, index, values) {
                    return moment(value, 'X').format(DATE_TIME);
                },
            }
        }],
        yAxes: [{
            stacked: true,
            ticks: {
                beginAtZero: true
            }
        }]
    },
    animation: false,
    tooltips: {
        enabled: true,
        callbacks: {
            label: function(tooltipItems) {
                return tooltipItems.yLabel || '0';
            },
            title: function(tooltipItems) { 
                return moment(tooltipItems[0].xLabel, 'X').format(DATE_TIME);
            }
        }
    }
};

let movieId;

class Statistic extends Component {

    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {}

    componentDidUpdate() {
        if (movieId != this.props.movieId) {
            movieId = this.props.movieId;
            this.getData();
        }

        this.updateChart();
    }

    getData() {
        let data = {
            movieId: movieId,
            fromTs: moment().format('X'),
            toTs: moment().format('X')
        };

        this.props.getMovieData(data);

        return true;
    }

    updateChart() {
        let newData = this.props.statistic;
        let cleanData;

        if (newData.length) {
            cleanData  = this.processData(newData);
        }

        this.chart_1 = this.refs['chart_1'].getChart();

        this.chart_1.data.datasets[0].data = cleanData.negative;
        this.chart_1.data.datasets[1].data = cleanData.positive;
        this.chart_1.update();
    }

    processData(data) {
        let cleanData = {
            positive: [],
            negative: []
        };

        let positiveSum = 0;
        let negativeSum = 0;

        data.map((el, i) => {
            let pItem = {};
            let nItem = {};

            positiveSum += parseInt(el.tweetCounterPositive, 10);
            negativeSum += parseInt(el.tweetCounterNegative, 10);

            pItem.x = el.timestamp;
            pItem.y = positiveSum;

            nItem.x = el.timestamp;
            nItem.y = negativeSum;

            cleanData.positive.push(pItem);
            cleanData.negative.push(nItem);
        });

        return cleanData;
    }

    render() {
        return (
            <div className="chart-1-box">
                <label className="title">Live chart</label>
                <RC2 ref='chart_1' data={ chartData } options={ chartOptions } type='line' />
            </div>
        )
    }

}

function mapStateToProps(state) {
    return {
        movieId: state.movieList.selected,
        statistic: state.statistic.statistic
    }
}

export default connect(
    mapStateToProps
)(Statistic)
