import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import RC2 from 'react-chartjs2';
import ChartHelper from '../helpers/ChartHelper';

let len = 0;

class Chart_bubble extends Component {

    constructor(props, context) {
        super(props, context);

        this.chartOption = ChartHelper.getBubbleOptions();
        this.chartDataset = ChartHelper.datasetBubble();
        this.processData = ChartHelper.processBubbleData;
    }

    componentDidUpdate() {
        return this.updateChart();
    }

    updateChart() {
        if (!this.props.movieData.length) {
            return false;
        }

        if (this.props.movieData.length !== len) {
            len = this.props.movieData.length;

            let cleanData = this.processData(this.props.movieData);

            this.chart_bubble = this.refs['chart_bubble'].getChart();

            this.chart_bubble.data.datasets[0].data = cleanData.negative;
            this.chart_bubble.data.datasets[1].data = cleanData.positive;
            this.chart_bubble.update();

            return true;
        }

        return false;
    }

    render() {

        return (
            <div className="chart_bubble-box">
                <h2 className="center">Bubble</h2>
                <RC2 ref='chart_bubble' data={ this.chartDataset } options={ this.chartOption } type='bubble' />
            </div>
        )
    }
}

export default Chart_bubble;
