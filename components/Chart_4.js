import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import RC2 from 'react-chartjs2';
import ChartHelper from '../helpers/ChartHelper';

let len = 0;

class Chart_4 extends Component {

    constructor(props, context) {
        super(props, context);

        this.chartOption = ChartHelper.getBarOptions();
        this.chartDataset = ChartHelper.datasetBar();
        this.processData = ChartHelper.processBarData;
    }

    componentDidUpdate() {
        return this.updateChart();
    }

    updateChart() {
        if (!this.props.movieData.length) {
            return false;
        }

        if (this.props.movieData.length !== len) {
            len = this.props.movieData.length;

            let cleanData = this.processData(this.props.movieData);

            this.chart_4 = this.refs['chart_4'].getChart();

            this.chart_4.data.datasets[0].data = cleanData.negative;
            this.chart_4.data.datasets[1].data = cleanData.positive;
            this.chart_4.data.labels = cleanData.labels;
            this.chart_4.update();

            return true;
        }

        return false;
    }

    render() {

        return (
            <div className="chart-4-box">
                <h2 className="center">Stacked bar</h2>
                <RC2 ref='chart_4' data={ this.chartDataset } options={ this.chartOption } type='bar' />
            </div>
        )
    }
}

export default Chart_4;
