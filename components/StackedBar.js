import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import RC2 from 'react-chartjs2';

let chartData = {
    datasets: [
        {
            label: 'Negative',
            data: [{x: moment().format('X'), y: 0}],
            lineTension: 0,
            pointRadius: 3,
            backgroundColor: 'rgba(255, 7, 23, 0.30)',
        },
        {
            label: 'Positive',
            data: [{x: moment().format('X'), y: 0}],
            lineTension: 0,
            pointRadius: 3,
            backgroundColor: 'rgba(20, 163, 255, 0.30)'
        }
    ]
};

let chartOptions = {
    responsive: true,
    scales: {
        xAxes: [{
            type: 'linear',
            position: 'bottom',
            ticks: {
                callback: function(value, index, values) {
                    return moment(value, 'X').format(DATE_MONTH);
                },
            }
        }],
        yAxes: [{
            stacked: true,
            ticks: {
                beginAtZero: true
            }
        }]
    },
    animation: false,
    tooltips: {
        enabled: true,
        callbacks: {
            label: function(tooltipItems) {
                return tooltipItems.yLabel || '0';
            },
            title: function(tooltipItems) {
                return moment(tooltipItems[0].xLabel, 'X').format(DATE_MONTH);
            }
        }
    }
};

let len = 0;

class StackedBar extends Component {
    constructor(props, context) {
        super(props, context);
    }

    componentDidUpdate() {
        return this.updateChart();
    }

    updateChart() {
        if (!this.props.movieData.length) {
            return false;
        }

        if (this.props.movieData.length !== len) {
            len = this.props.movieData.length;

            let cleanData = this.processData(this.props.movieData);

            this.chart_4 = this.refs['chart_4'].getChart();

            this.chart_4.data.datasets[0].data = cleanData.negative;
            this.chart_4.data.datasets[1].data = cleanData.positive;
            this.chart_4.update();

            return true;
        }

        return false;
    }

    processData(data) {
        let cleanData = {
            positive: [],
            negative: []
        };

        let positiveSum = 0;
        let negativeSum = 0;

        data.map((el, i) => {
            let pItem = {};
            let nItem = {};

            positiveSum += parseInt(el.tweetCounterPositive, 10);
            negativeSum += parseInt(el.tweetCounterNegative, 10);

            pItem.x = el.timestamp;
            pItem.y = positiveSum;

            nItem.x = el.timestamp;
            nItem.y = negativeSum;

            cleanData.positive.push(pItem);
            cleanData.negative.push(nItem);
        });

        return cleanData;
    }

    render() {

        return (
            <div className="chart-42-box">
                <h2>Stacked bar2</h2>
                <RC2 ref='chart_42' data={ chartData } options={ chartOptions } type='line' />
            </div>
        )
    }
}

export default StackedBar;
