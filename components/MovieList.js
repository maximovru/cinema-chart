import React, { Component, PropTypes } from 'react';

class MovieList extends Component {

    constructor(props, context) {
        super(props, context);

        this.props.getMovieList();
    }

    checkMovie(event) {
        return this.props.selectMovie(event.target.value);
    }

    render() {
        const {
            movies,
            selected
        } = this.props;

        return (
            <div>
                <label className="title">Movies</label>
                <div className="half-block">
                    <table className="movie-list" width="100%">
                        <thead>
                            <tr>
                                <th width="*">name</th>
                                <th width="50px">IMDb</th>
                                <th width="90px">release</th>
                            </tr>
                        </thead>
                        <tbody>
                            {movies.map((movie, i) => (
                                <tr key={ movie.id }>
                                    <td>
                                        <label className="label">
                                            <input type="radio" name="movies"  value={ movie.id } onChange={ this.checkMovie.bind(this) } checked={ selected == movie.id }/>
                                            <span className="right">{ movie.name }</span>
                                        </label>
                                    </td>
                                    <td>
                                        { movie.imdb.toFixed(1) }
                                    </td>
                                    <td>
                                        { movie.releaseDate }
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

MovieList.propTypes = {
    movies: PropTypes.array.isRequired,
    // getUsersList: PropTypes.func
}

export default MovieList;
