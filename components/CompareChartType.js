import React, { Component, PropTypes } from 'react';

class ChartType extends Component {

    constructor(props, context) {
        super(props, context);
    }

    selectChartType(evnt) {
        this.props.selectChartType(evnt.target.value);
    }

    render() {
        const selected = this.props.selectedChartType;

        return (
            <div>
                <h2>Select the chart type</h2>
                <div className="half-block">
                    <ul className="chart-type-list">
                        <li>
                            <label className="label">
                                <input
                                    type="radio"
                                    checked={ selected == 'line_all' }
                                    name="type-list"
                                    value="line_all"
                                    onChange={ this.selectChartType.bind(this) }
                                />
                                <span className="right">Stacked area</span>
                            </label>
                        </li>
                        <li>
                            <label className="label">
                                <input
                                    type="radio"
                                    checked={ selected == 'line' }
                                    name="type-list"
                                    value="line"
                                    onChange={ this.selectChartType.bind(this) }
                                />
                                <span className="right">Stacked area (positive/negative only)</span>
                            </label>
                        </li>
                        <li>
                            <label className="label">
                                <input
                                    type="radio"
                                    checked={ selected == 'bar' }
                                    name="type-list"
                                    value="bar"
                                    onChange={ this.selectChartType.bind(this) }
                                />
                                <span className="right">Stacked bar</span>
                            </label>
                        </li>
                        <li>
                            <label className="label">
                                <input
                                    type="radio"
                                    checked={ selected == 'bubble' }
                                    name="type-list"
                                    value="bubble"
                                    onChange={ this.selectChartType.bind(this) }
                                />
                                <span className="right">Bubble</span>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default ChartType;
