import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import RC2 from 'react-chartjs2';
import { DATE_MONTH } from '../constants/Variable';
import ChartHelper from '../helpers/ChartHelper';

let len = 0;

class Chart_1 extends Component {

    constructor(props, context) {
        super(props, context);

        this.chartOption = ChartHelper.getLineOptions();
        this.chartDataset = ChartHelper.datasetLine();
        this.processData = ChartHelper.processLineData;

        this.chartOption.onClick = this.getDataPoint.bind(this);
    }

    componentDidUpdate() {
        return this.updateChart();
    }

    getDataPoint(event, points) {
        if (points.length) {
            let index = points[0]._index;
            let data = points[0]._chart.config.data.datasets[0].data[index];
            console.log('tada: ', data);

            this.props.selectPoint({
                movie: this.props.movieId,
                fromTs: data.x,
                toTs: data.x + data.period,
                perPage: 10,
                page: 1
            });
        }
    }

    updateChart() {
        if (!this.props.movieData.length) {
            return false;
        }

        if (this.props.movieData.length !== len) {
            len = this.props.movieData.length;

            let cleanData = this.processData(this.props.movieData);

            this.chart_1 = this.refs['chart_1'].getChart();

            this.chart_1.data.datasets[0].data = cleanData.negative;
            this.chart_1.data.datasets[1].data = cleanData.positive;
            this.chart_1.update();

            return true;
        }

        return false;
    }

    render() {

        return (
            <div className="chart-1-box">
                <h2>Chart positive/negative tweets for timeline</h2>
                <RC2 ref='chart_1' data={ this.chartDataset } options={ this.chartOption } type='line' />
            </div>
        )
    }
}

export default Chart_1;
