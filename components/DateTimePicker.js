/**
 * Created by omaximov on 14/09/16.
 */
import React, { Component, PropTypes } from 'react';
import Chart from 'vendor/datetimepicker.js'
import moment from 'moment';

let dtp;

class DateTimePicker extends Component {
    constructor(props, context) {
        super(props, context);
    }
    componentDidUpdate() {
        if (!chart) {
            this.buildChart();
        } else {
            this.updateChart();
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.movieData.length == this.props.movieData.length) {
            let newStr = JSON.stringify(nextProps.movieData[0]);
            let oldStr = JSON.stringify(this.props.movieData[0]);

            return newStr !== oldStr;
        }

        return true;
    }

    updateChart() {
        let cleanData = this.processData(this.props.movieData);

        chart.data.datasets[0].data = cleanData.negative;
        chart.data.datasets[1].data = cleanData.positive;
        chart.update();
    }

    buildChart() {
        let data = this.props.movieData;

        if (!data.length) { return false; }

        let ctx = document.getElementById("chart_2");
        let cleanData = this.processData(data);

        chart = new Chart(ctx, {
            type: 'line',
            data: {
                xLabels: cleanData.labels,
                datasets: [
                    {
                        label: 'Negative',
                        data: cleanData.negative,
                        backgroundColor: 'rgba(255, 7, 23, 0.30)'
                    },
                    {
                        label: 'Positive',
                        data: cleanData.positive,
                        backgroundColor: 'rgba(20, 163, 255, 0.30)'
                    }
                ]
            },
            options: {
                scales: {
                    xAxes: [{
                        type: 'linear',
                        position: 'bottom'
                    }]
                }
            }
        });
    }

    render() {

        return (
            <input type="text" value="" id="datetimepicker"/>
        )
    }
}

export default DateTimePicker;