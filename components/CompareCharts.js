import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import RC2 from 'react-chartjs2';
import ChartHelper from '../helpers/ChartHelper';

class CompareCharts extends Component {

    constructor(props, context) {
        super(props, context);

        this.chartsTitle = [];

        this.chartsTitle.push('1');
        this.chartsTitle.push('2');

        this.chartOption = ChartHelper.getLineOptions();
        this.chartDataset = ChartHelper.datasetLine();
        this.processData = ChartHelper.processLineData;

        this.chartType = this.props.selectedChartType === 'line_all' ? 'line' : this.props.selectedChartType;
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.selectedChartType !== this.props.selectedChartType) {
            switch (nextProps.selectedChartType) {
                case 'line':
                    this.chartOption = ChartHelper.getLineOptions();
                    this.chartDataset = ChartHelper.datasetLine();
                    this.processData = ChartHelper.processLineData;
                    this.chartType = 'line';
                    break;

                case 'line_all':
                    this.chartOption = ChartHelper.getLineOptions();
                    this.chartDataset = ChartHelper.datasetLine();
                    this.processData = ChartHelper.processLineData;
                    this.chartType = 'line';
                    break;

                case 'bar':
                    this.chartOption = ChartHelper.getBarOptions();
                    this.chartDataset = ChartHelper.datasetBar();
                    this.processData = ChartHelper.processBarData;
                    this.chartType = 'bar';
                    break;

                case 'bubble':
                    this.chartOption = ChartHelper.getBubbleOptions();
                    this.chartDataset = ChartHelper.datasetBubble();
                    this.processData = ChartHelper.processBubbleData;
                    this.chartType = 'bubble';
                    break;
            }
            return true;
        }

        let keysFirst = Object.keys(nextProps.movieDataMap);
        let keysSecond = Object.keys(this.props.movieDataMap);

        if (keysFirst.toString() !== keysSecond.toString()) {
            return true;
        }

        return false;
    }

    componentDidUpdate() {
        return this.updateCharts(this.props.movieDataMap);
    }

    updateCharts(data) {
        let i = 1;
        for (let key in data) {
            let cleanData = this.processData(data[key]);
            let chart = this.refs['charts' + i].getChart();
            chart.data.datasets[0].data = cleanData.negative;
            chart.data.datasets[1].data = cleanData.positive;
            if (cleanData.hasOwnProperty('labels')) {
                chart.data.labels = cleanData.labels;
            }
            chart.update();
            i++;
        };
        return true;
    }

    render() {
        let titles = [];

        for (let key in this.props.movieDataMap) {
            titles.push(key);
        }

        return (
            <div className="chart-2-box">
                <h2 className="center">{ titles[0] }</h2>
                <RC2 ref='charts1' data={ this.chartDataset } options={ this.chartOption } type={ this.chartType } />
                <h2 className="center">{ titles[1] }</h2>
                <RC2 ref='charts2' data={ this.chartDataset } options={ this.chartOption } type={ this.chartType } />
            </div>
        )
    }
}

export default CompareCharts;
